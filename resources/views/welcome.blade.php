<!DOCTYPE html>

<html>
    <head>
        <title>Moneris Form</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

        <style>
         /*  *{
                border:solid red; 
            }*/
            html, body {
                height: 100%;
            }

            body {
                color:#4b7196;
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: right;
                /*display: table-cell;*/
                vertical-align: middle;
                padding:5%;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                padding:5%;
                font-size: 28px;
                text-align:center;
                font-weight:900;
                
            }

            .column1{
                height:42px;
                font-size: 28px;
            }
            .column2{
                width:100%;
                height:42px;
                
            }
            
            .instructions{
                text-align: left;
                font-size: 13.5px;
                padding-bottom: 5px;
            }
            
        </style>
    </head>
    <body>
   <div class="page-header title">
      Sample Payment Processing with Moneris Integration
   </div>
   <div class="container">
      <div class="row">
         <div class="col-lg-2 offset-lg-2 column1">
            Order #
         </div>
         <div class="col-lg-4">
            <input type="text" class="column2">
         </div>
         <div class="col-lg-4 instructions">
            From the Moneris package provided 
            use the Examples/CA/TestPurchase.php 
            to build the following integration application
         </div> 
      </div>

      <div class="row">
         <div class="col-lg-2 offset-lg-2 column1">
            Order Total
         </div>
         <div class="col-lg-4">
            <input type="text" class="column2">
         </div>
         <div class="col-lg-4 instructions">
            
         </div> 
      </div>

      <div class="row">
         <div class="col-lg-2 offset-lg-2 column1">
            Card Type
         </div>
         <div class="col-lg-4">
            <input type="text" class="column2">
         </div>
         <div class="col-lg-4 instructions">
            We would like the application in Angular
            with the PHP supporting Moneris
            integration.
         </div> 
      </div>

      <div class="row">
         <div class="col-lg-3 offset-lg-1 column1">
            Credit Card #
         </div>
         <div class="col-lg-4">
            <input type="text" class="column2">
         </div>
         <div class="col-lg-4 instructions">
            
         </div> 
      </div>

      <div class="row">
         <div class="col-lg-3 offset-lg-1 column1">
            Name on Card
         </div>
         <div class="col-lg-4">
            <input type="text" class="column2">
         </div>
         <div class="col-lg-4 instructions">
            There should be validation on the fields
            In terms of integers, required, etc.
            To avoid submission errors to Moneris
         </div> 
      </div>

      <div class="row">
         <div class="col-lg-2 offset-lg-2 column1">
            CVD
         </div>
         <div class="col-lg-4">
            <input type="text" class="column2">
         </div>
         <div class="col-lg-4 instructions">
           
         </div> 
      </div>

      <div class="row">
         <div class="col-lg-4 column1">
            Expiry month/ Year
         </div>
         <div class="col-lg-2">
            <input type="text" class="column2">
         </div>
         <div class="col-lg-2">
            <input type="text" class="column2">
         </div>
         <div class="col-lg-4 instructions">
            
         </div> 
      </div>


      <form>
         <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
         </div>
         <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
         </div>
         <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Check me out</label>
         </div>
         <button type="submit" class="btn btn-primary">Submit</button>
      </form>
   </div>
   <script src="{{ asset('js/bootstrap.min.js') }}"></script>
</body>
</html>
